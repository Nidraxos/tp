# TP

# I. Exploration locale en solo

* Carte réseau sans fil Wi-fi (Intel(R) Wireless-AC 9560), A4-C3-F0-37-3B-56, 10.33.1.155
* Carte ethernet (8C-04-BA-98-AC-E0), 
* 'ip config /all', 10.33.3.253
* En bas à droite de l'ecran, cliquer (clique droit) sur le logo du wi-fi ou ethernet, aller dans 'ouvrir les paramètres reseau et internet', cliquer sur le wi-fi (ou ethernet) puis aller en bas, et nous voyons les propriétés.
* La gateway permet de se connecter aux réseaux exterieur.

# 2. Modifications des informations
## A. Modification d'adresse IP (part 1)


![](https://i.imgur.com/PcefdQP.png)


* Un conflit apparaît car je peux posséder la meme ip que quelqu'un d'autre.

## B. NMAP

## C. Modification d'adresse IP (part 2)

* C'est le même principe que : 
A. Modification d’adresse IP (part 1)
(Je n'ai pas pus utiliser nmap)

## II. Exploration locale en duo

Groupe : Richard, Ford, Texier, David

* Apres avoir désactiver les firewalls et en reliant les 2 pc par cable ethernet, nous avons modifiers les ip pour qu'ils soient dans le meme réseau local, le pc1 est en 192.168.0.1 et le pc2 est en 192.168.0.2 avec un masque en /30 ( 255.255.255.252) et pour le pc1 ( le receveur de la connexion partagé en gateway il a le pc 2 donc 192.168.0.2)
* En faisait un ip config on voit bien que les changement on été effectuer et en effectuant un ping les 2 pc se ping.
* Résultat du ipconfig PC1 : Carte réseau Ethernet :

   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.0.2
   
* Résultat du ipconfig PC2 : Carte réseau Ethernet :

   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   
* Résultat du ping : Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données :
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.0.2:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 1ms, Moyenne = 1ms
* Après avoir désactiver la carte wifi du pc1, nous avons configurer le partage de connexion sur le pc2 de la facon suivante : Réseau et Internet -> "modifier les options de l'adaptateur" -> séléctionner la carte réseau qui partage la connexion ( carte wifi) -> "partage" -> "autoriser les utilisateur du réseau a se connecter via la connexion internet de cet ordinateur" -> selectioner par quel carte réseau le partage se fait ( Ethernet) -> OK.
* Il faut changer maintenant l'adresse ip du pc1 car par cette methode l'ip du pc2 a changer en 192.168.137.1
* Donc on change l'ip du pc2 en 192.168.137.2/30 et en utilisant l'ip du pc2 comme gateway.
* Et par ce procéder le pc1 a internet.
* La commande "curl google.com" fonctionne.
* En faisant un tracert -d 8.8.8.8 on voit bien que : Détermination de l’itinéraire vers 8.8.8.8 avec un maximum de 30 sauts.

  1     1 ms    <1 ms    <1 ms  192.168.137.1
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     7 ms     7 ms    11 ms  10.33.3.253
  
  ### Petit chat privé
  
  *PS C:\Users\6237675\Desktop\ .\nc.exe -l -p 10000
bobo
hi man

 * PS C:\Users\fmazurie\Desktop\ .\nc.exe 192.168.137.1 10000
bobo
hi man

 ### Wireshark
 
 ![](https://i.imgur.com/fqUUqAU.png)
 
 ### Firewall
 
 ![](https://i.imgur.com/kEzABrV.png)


# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

*  Serveur DHCP : 10.33.3.254
*  Bail obtenu : jeudi 16 janvier 2020 16:22:17
   Bail expirant : jeudi 16 janvier 2020 17:52:26
* Faire un ipconfig /release puis un ipconfig /renew

## 2. DNS

* Serveur DNS : 10.33.10.20
                                       10.33.10.2
                                       8.8.8.8
                                       8.8.4.4

* nslookup google.com :
Réponse ne faisant pas autorité :
Nom :    google.com
Addresses: 216.58.201.238

* nslookup ynov.com : 
Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  217.70.184.38

* 92.146.54.88 : apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
* 78.74.21.21 : host-78-74-21-21.homerun.telia.com




